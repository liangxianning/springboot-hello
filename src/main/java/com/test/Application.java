package com.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
public class Application {

    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public String hello() {
        return "Hello World! test springboot jar run";
    }

    @RequestMapping(value = "/hello2", method = RequestMethod.GET)
    public String helloWho(@RequestParam("name") String name) {
        return "hello "+name;
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}